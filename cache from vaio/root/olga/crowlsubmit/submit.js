var start = new Date().getTime();
var page = require('webpage').create();
var system = require('system');
var testindex=0, loadInProgress=false;
var host = "http://runa.ru";
//var url = host +"/%D0%A2%D0%B5%D1%81%D1%82";
var url = host +"/"+encodeURIComponent(system.args[1]);
var div_content_id="runa-second_content";

page.onConsoleMessage = function(msg) {
 if (msg.indexOf("Unsafe JavaScript attempt to access frame with URL") > -1)
    return; 
    console.log(msg);
};
page.onLoadStarted = function() {
  loadInProgress = true;
  //console.log("load started");
};
page.onLoadFinished = function() {
  loadInProgress = false;
  //console.log("load finished");
};

var openurl = function (){
	page.open(url, function(){page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js");});
		return -1;
}
var getforms = function(){
	return page.evaluate(function(){
			return $('form').length;
	});
}
var submit = function(i){
	page.evaluate(function(i) {
		var form = $('form:eq('+i+')');
		$(form).find(":input[name^='form_']").not("[name^='form_start_']").each(function(index){
				
			if ($(this).attr('name')=='form_%D0%9A%D0%BE%D0%B4+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B0' ||$(this).attr('name')=='form_%D0%9A%D0%BE%D0%B4') {
				$(this).val('123');
			}
			else if ($(this).attr('name')=='form_%D0%A2%D0%B5%D0%BB%D0%B5%D1%84%D0%BE%D0%BD') {
				$(this).val('1234567');
			}
			else if ($(this).attr('name')=='form_e-mail') {
				$(this).val('butygina_o@runa.ru');
			} 
			else if ($(this).attr('name')=='form_%E2%84%96+%D0%B4%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80%D0%B0') {
			   $(this).val('11111');
			}
			else $(this).val('Test');
					
		});
		$(form).find(":input[name='to']").val('butygina_o@runa.ru');
		$(form).submit();
		console.log('form ' + $(form).attr('name') + ' submitted');
	},i);
	return -4;
}
var output = function(){
	page.evaluate(function(div_content_id) {
		console.log(document.getElementById(div_content_id).innerText);
	}, div_content_id);	
	return -5;
}


var steps = [openurl,getforms];

interval = setInterval(function() {

  if (!loadInProgress && (typeof steps[testindex] == "function") ) {
	var formIndex = 0;
	formIndex = steps[testindex]((testindex-3)/3); 
 	
	if (formIndex>0) {
	    console.log(formIndex + ' forms on a page '+decodeURIComponent(url));
		for (var i = 0; i < formIndex; ++i) {
			steps.push(openurl);
			steps.push(submit);
			steps.push(output);
		}
	}
	 testindex++;
  }
  else if (typeof steps[testindex] != "function" ) {
    console.log("Job complete!");
	var end = new Date().getTime();
	var time = end - start;
	console.log('Execution time: ' + time/1000+' seconds');
	phantom.exit();
  }
}, 100);