    var querystring = require('querystring');  
    var http = require('http');  
      
    var post_domain = 'www.runa.ru';  
    var post_port = 80;  
    var post_path = '/%D0%A2%D0%B5%D1%81%D1%82';  
      
    var post_data = querystring.stringify({  
		'form_e-mail':    'karagota@gmail.com',
		'form_%D0%98%D0%BC%D1%8F':    'Chuck Norris',
		'form_%D0%9A%D0%BE%D0%B4+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B0':   '000',
		'form_%D0%A2%D0%B5%D0%BB%D0%B5%D1%84%D0%BE%D0%BD':       '0000000',
		'form_%D0%9E%D1%80%D0%B3%D0%B0%D0%BD%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F':      'runa'
    });  
      
    var post_options = {  
      host: post_domain,  
      port: post_port,  
      path: post_path,  
      method: 'POST',  
      headers: {  
        'Content-Type': 'application/x-www-form-urlencoded',  
        'Content-Length': post_data.length  
      }  
    };  
      
    var post_req = http.request(post_options, function(res) {  
      res.setEncoding('utf8');  
      res.on('data', function (chunk) {  
        console.log('Response: ' + chunk);  
      });  
    });  
      
    // write parameters to post body  
    post_req.write(post_data);  
    post_req.end();  