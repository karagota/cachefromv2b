var page = require('webpage').create();
var testindex=1, loadInProgress=false;
Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};
page.onConsoleMessage = function(msg) {
 if (msg.indexOf("Unsafe JavaScript attempt to access frame with URL") > -1)
    return; 
    console.log(msg);
};
page.onLoadStarted = function() {
  loadInProgress = true;
  //console.log("load started");
};
page.onLoadFinished = function() {
  loadInProgress = false;
  //console.log("load finished");
};

var url = "runa.ru/%D0%A2%D0%B5%D1%81%D1%82";
var div_content_id="runa-second_content";

var openurl = function (){
	page.open("http://"+url);
		return -1;
}
var includejs = function(){
   page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js");
		return -2;
}
var getforms = function(){
	return page.evaluate(function(){
			return $('form').length;
	});
}

var foo = function(){
	console.log("foo called");
	return -3;
}

var submit = function(i){
	page.evaluate(function(i) {
		var form = $('form:eq('+i+')');
		$(form).find(":input[name^='form_']").not("[name^='form_start_']").each(function(index){
				
			if ($(this).attr('name')=='form_%D0%9A%D0%BE%D0%B4+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B0' ||$(this).attr('name')=='form_%D0%9A%D0%BE%D0%B4') {
				$(this).val('123');
			}
			else if ($(this).attr('name')=='form_%D0%A2%D0%B5%D0%BB%D0%B5%D1%84%D0%BE%D0%BD') {
				$(this).val('1234567');
			}
			else if ($(this).attr('name')=='form_e-mail') {
				$(this).val('butygina_o@runa.ru');
			} 
			else if ($(this).attr('name')=='form_%E2%84%96+%D0%B4%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80%D0%B0') {
			   $(this).val('11111');
			}
			else $(this).val('Test');
					
		});
		$(form).find(":input[name='to']").val('butygina_o@runa.ru');
		$(form).submit();
		console.log('form ' + $(form).attr('name') + ' submitted');
	},i);
	return -4;
}
var output = function(){
	page.evaluate(function(div_content_id) {
		//console.log('Output content of page to stdout after form has been submitted');
		console.log(document.getElementById(div_content_id).innerText);
	}, div_content_id);	
	return -5;
}
var getlinks = function(){
	return page.evaluate(function(url){
			return Array.prototype.slice.call($('a[href^="/"],[href^="http://www."'+url+'"],[href^="http://"'+url+'"]'), 0)
			.map(function (link) {
			return link.getAttribute("href");
			});
	});
	
}
var steps = [foo,openurl,includejs,getlinks,getforms];

interval = setInterval(function() {

  if (!loadInProgress && typeof steps[testindex] == "function") {
    var formIndex = steps[testindex]((testindex-7)/4); 
    testindex++;
	
	if( typeof(formIndex) == 'object' )
	{
		console.log('The number of links on page '+url+' equals '+formIndex.length);
		
		var out='';
		for (property in formIndex) {
				
				out += property + ': ' + formIndex[property]+"; \n";
		}
		console.log (out);
		
	}
	
	else if (formIndex>0) {
	    console.log('The number of forms on page '+url+' equals '+formIndex);
		
		for (var i = 0; i < formIndex; ++i) {
		    
			steps.push(openurl);
			steps.push(includejs);
			steps.push(submit);
			steps.push(output);
		}
	}
  }
  if (typeof steps[testindex] != "function") {
    console.log("Job complete!");
	phantom.exit();
  }
}, 200);