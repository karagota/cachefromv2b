var page = new WebPage(),
     url = 'http://runa.ru/%D0%A2%D0%B5%D1%81%D1%82',
     stepIndex = 0;
	 forms = {};

Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};
 
 /**
  * From PhantomJS documentation:
  * This callback is invoked when there is a JavaScript console. The callback may accept up to three arguments: 
  * the string for the message, the line number, and the source identifier.
  */
 page.onConsoleMessage = function (msg, line, source) {
	if (msg.indexOf("Unsafe JavaScript attempt to access frame with URL") > -1)
		return; 
    console.log('console> ' + msg);
 };
 
 /**
  * From PhantomJS documentation:
  * This callback is invoked when there is a JavaScript alert. The only argument passed to the callback is the string for the message.
  */
 page.onAlert = function (msg) {
     console.log('alert!!> ' + msg);
 };
 
 // Callback is executed each time a page is loaded...
 page.open(url, function (status) {
   if (status === 'success') {
     // State is initially empty. State is persisted between page loads and can be used for identifying which page we're on.
     console.log('============================================');
     console.log('Step "' + stepIndex + '"');
     console.log('============================================');
 
     // Inject jQuery for scraping (you need to save jquery-1.6.1.min.js in the same folder as this file)
    // page.injectJs('jquery-1.6.1.min.js');
	page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js");
 
     // Our "event loop"
     if(!phantom.state){
       initialize();
     } else {
       phantom.state();
     } 
 
     // Save screenshot for debugging purposes
     //page.render("step" + stepIndex++ + ".png");
   }
 });
 
 // Step 1
 function initialize() {
   page.evaluate(function(forms) {
		$("form").each(function(index){
			$(this).find(":input[name^='form_']").not("[name^='form_start_']").each(function(index){
				if ($(this).attr('name')=='form_%D0%9A%D0%BE%D0%B4+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B0') {
					$(this).val('123');
				}
				else if ($(this).attr('name')=='form_%D0%A2%D0%B5%D0%BB%D0%B5%D1%84%D0%BE%D0%BD') {
					$(this).val('1234567');
				}
				else if ($(this).attr('name')=='form_e-mail') {
					$(this).val('karagota@gmail.com');
				} 
				else if ($(this).attr('name')=='form_%E2%84%96+%D0%B4%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80%D0%B0'){
				   $(this).val('11111');
				}
				else $(this).val('Тест2');
			});
			
			var len = Object.size(forms);
			forms[len]={};
			$(this).find(":input").each(function(index){
				forms[len][$(this).attr('name')]=$(this).attr('value');
			});
		});
},forms);
   // Phantom state doesn't change between page reloads
   // We use the state to store the search result handler, ie. the next step
   phantom.state = submitForm0; 
 }
 
 // Step 2
function submitForm0() {
   page.evaluate(function(forms) {
		var form = $('form:eq('+forms[0]+')');
		var formname = $(form).attr('name');
		for (var name in forms[0]) {
			$(form).find(':input[name='+name+']').first().val(forms[0][name]);
		}
		$(form).submit(); 
		console.log('form ' + formname + ' submitted');},forms);
   });
   phantom.state = submitForm1; 
}

 // Step 3
function submitForm1() {
   data = (forms[1].serialize());//'universe=expanding&answer=42';

	page.open(url, 'post', data, function (status) {
		if (status !== 'success') {
			console.log('Unable to post!');
		} else {
			console.log(page.content);
		}
		phantom.exit();
	});
}