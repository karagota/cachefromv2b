var start = new Date().getTime();
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var host='runa.ru';
if(!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(needle) {
        for(var i = 0; i < this.length; i++) {
            if(this[i] === needle) {
                return i;
            }
        }
        return -1;
    };
}
var links = [];
var links_visited={};
var visited_arr = [];
var getlinks = function(error, response, body){
	if (body!=undefined) {
	$ = cheerio.load(body.toString());
	//searching only this site links
	//$('a[href^="/"],[href^="http://www."'+host+'"],[href^="http://"'+host+'"]').each(function(idx,elem) {
	$('a').each(function(idx,elem) {
		var new_link =$(elem).attr('href');
		if (new_link!= undefined) {
			if (new_link.substring(0,1)=='/'  || new_link.substring(0,18)=='http://www.runa.ru')
			
			if (new_link.substring(0,2)!='//' && new_link!='http://www.runa.ru' && new_link.substring(0,23)!='http://www.runa.ru/wiki'&& new_link.substring(0,26)!='http://www.runa.ru/images/' && new_link.substring(0,20)!='/Mediawiki:Common.js' && new_link.substring(0,81)!= '/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:UserLogin' && !links_visited[new_link])
			{	
				console.log(new_link);
				if (links.indexOf(new_link)==-1)
					links.push(new_link);
			}
		}
	});}
	console.log('links='+links.length+', links_visited='+visited_arr.length);
	if (!(link =  links.pop())) {
	
		fs.writeFile("output-runa", visited_arr.join("\n"), function(err) {
			if(err) {console.log(err);} else {console.log("The file was saved!");}});
		var end = new Date().getTime();
		var time = end - start;
		console.log('Execution time: ' + time/1000+' seconds');
		return;
	}
	while (links_visited[link]==1) {
		if (!(link =  links.pop())) return; //base case
	}
	links_visited[link]=1;
	visited_arr.push(decodeURIComponent(link));
	if (link.substring(0,1)=='/') link='http://'+host+link;
	request(link,getlinks); //recursion
}
request('http://'+host, getlinks);