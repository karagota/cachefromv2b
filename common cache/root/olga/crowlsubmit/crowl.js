var start = new Date().getTime();
var page = require('webpage').create();
var testindex=0, loadInProgress=false;

page.onConsoleMessage = function(msg) {
 if (msg.indexOf("Unsafe JavaScript attempt to access frame with URL") > -1)
    return; 
    console.log(msg);
};
page.onLoadStarted = function() {
  loadInProgress = true;
  //console.log("load started");
};
page.onLoadFinished = function() {
  loadInProgress = false;
  //console.log("load finished");
};
var host = "runa.ru";
var url = host +"/%D0%A2%D0%B5%D1%81%D1%82";
var links=[url];
var cururls=[url];
var div_content_id="runa-second_content";

var openurl = function (cururl){
    console.log('open page '+decodeURIComponent(cururl));
	page.open("http://"+cururl, function(){page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js");});
		return -1;
}
var getforms = function(){
	return page.evaluate(function(){
			return $('form').length;
	});
}
var submit = function(i){
	page.evaluate(function(i) {
		var form = $('form:eq('+i+')');
		$(form).find(":input[name^='form_']").not("[name^='form_start_']").each(function(index){
				
			if ($(this).attr('name')=='form_%D0%9A%D0%BE%D0%B4+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B0' ||$(this).attr('name')=='form_%D0%9A%D0%BE%D0%B4') {
				$(this).val('123');
			}
			else if ($(this).attr('name')=='form_%D0%A2%D0%B5%D0%BB%D0%B5%D1%84%D0%BE%D0%BD') {
				$(this).val('1234567');
			}
			else if ($(this).attr('name')=='form_e-mail') {
				$(this).val('butygina_o@runa.ru');
			} 
			else if ($(this).attr('name')=='form_%E2%84%96+%D0%B4%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80%D0%B0') {
			   $(this).val('11111');
			}
			else $(this).val('Test');
					
		});
		$(form).find(":input[name='to']").val('butygina_o@runa.ru');
		$(form).submit();
		console.log('form ' + $(form).attr('name') + ' submitted');
	},i);
	return -4;
}
var output = function(){
	page.evaluate(function(div_content_id) {
		console.log(document.getElementById(div_content_id).innerText);
	}, div_content_id);	
	return -5;
}
/*var getlinks = function(cururl){
	console.log('get links for page '+decodeURIComponent(cururl));
	page.open("http://"+cururl, function(){
		console.log('open page '+decodeURIComponent(cururl));
		page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js");
		var arr = page.evaluate(function(host,cururl){
			console.log('window.location='+window.location);
			window.location="http://"+cururl;
			console.log('window.location='+window.location);
			return Array.prototype.slice.call($('a[href^="/"],[href^="http://www."'+host+'"],[href^="http://"'+host+'"]'), 0)
			.map(function (link) {return link.getAttribute("href");});
		});
		for (key in arr) {
			console.log(key + ': ' + decodeURIComponent(arr[key])+"; \n");
		}
	});
}*/

var getlinks = function(){
	return page.evaluate(function(host){
		console.log('window.location='+window.location);
		return Array.prototype.slice.call($('a[href^="/"],[href^="http://www."'+host+'"],[href^="http://"'+host+'"]'), 0)
		.map(function (link) {return link.getAttribute("href");});
	});
}

var foo = function(){}

var steps = ['openurl',getforms,'getlinks'];

interval = setInterval(function() {

  if (!loadInProgress && (typeof steps[testindex] == "function" || steps[testindex]=='openurl' || steps[testindex]=='getlinks') && testindex<50) {
	var formIndex = 0;
	if (steps[testindex]=='openurl') {
	    console.log("cururls["+testindex+"]="+decodeURIComponent(cururls[testindex]));
		formIndex = openurl(cururls[testindex]);
	}
	else if (steps[testindex]=='getlinks') {
		formIndex = getlinks();
	}
	else formIndex = steps[testindex]((testindex-4)/3); 
   
	
	if( typeof(formIndex) == 'object' )
	{
		console.log('The number of links on page '+decodeURIComponent(cururls[testindex-2])+' equals '+formIndex.length);
		links = links.concat(formIndex);
		var out='';
		for (property in formIndex) {
			out += property + ': ' + decodeURIComponent(formIndex[property])+"; \n";
			steps.push('openurl');
			var c = formIndex[property];
			if (c.substring(0,1)=='/') c='http://'+host+c;
			cururls[steps.length-1]=c;
			steps.push(foo);
			steps.push('getlinks');
		}
		console.log (out);
	}
	
	else if (formIndex>0) {
	    console.log('The number of forms on page '+decodeURIComponent(cururls[testindex-1])+' equals '+formIndex);
		for (var i = 0; i < formIndex; ++i) {
			steps.push('openurl');
			cururls[steps.length-1]=url;
			steps.push(submit);
			steps.push(output);
		}
	}
	 testindex++;
  }
  else if (typeof steps[testindex] != "function" || testindex==50) {
    console.log("Job complete!");
	var end = new Date().getTime();
	var time = end - start;
	console.log('Execution time: ' + time/1000+' seconds');
	phantom.exit();
  }
}, 100);